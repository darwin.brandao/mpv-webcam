#!/bin/bash

# Variables
devices="$( find "/dev" -name "video*" )"
num_devices=$( wc -l <<< "$devices" )

device_menu=""
linecount=0

is_in_tty="$( tty -s; echo "$?" )"

# Functions
function display_menu(){
	stdin="$( cat - )"
	if [ "$is_in_tty" -eq 0 ]; then
		fzf --with-nth 2.. <<< "$stdin"
	else
		rofi -dmenu -i <<< "$stdin"
	fi
}

function list_devices(){
	# Loop devices
	while IFS= read line; do
	
	        ((linecount++))
	
	        # Get device type
	        dev_type="\t[ $( \
			v4l2-ctl --device="$line" --all | \
	                awk '/Device Caps/ { getline; sub(/^[ \t\r\n]+/, "", $0); print }' \
	        ) ]"

	        # Get device name
	        dev_name="$( \
	                v4l2-ctl --device="$line" --all | \
	                grep "Card type" | \
	                awk -F ": " '{ print $2 }' \
	        )"

		# Chek if it is not metadata device
		if [[ "$dev_type" == *"Video Capture"* ]]; then
		
			# Add video capture device to list
			webcams="${webcams}${line}\n"

			# Format device_menu
			device_menu+="${dev_name}${dev_type}"
			
			# Line Break
			[ "$linecount" -lt "$num_devices" ] &&
				device_menu+="\n"

		fi
	
	done <<< "$devices"

	# Select video device
	device_index=$( \
	        echo -e "$device_menu" | \
	        cat -n | \
	        display_menu | \
	        awk '{print $1}'
	)

	# Get the device from selected index
	device="$( awk -v i="$device_index" 'NR==i{print}' <<< "$( echo -e "$webcams" )" )"

	echo "$device"
}

list_devices
