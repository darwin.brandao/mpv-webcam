#!/bin/bash

# Directory of the script, not the working directory
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

size=30

function usage()
{
    echo "Usage: ${0} [OPTIONS] [-b VALUE]"
    echo "  -r      Resolution. Example: mpv-webcam -r \"width=X,height=Y\""
    echo "  -d      Webcam name"
    echo "  -f      Start fullscreen"
    echo "  -s      Window size"
    echo "  --vf    Video filters (ffmpeg)"
    echo "  --vff   Video filters (ffmpeg) when fullscreen"
    echo "  --vfw   Video filters (ffmpeg) when windowed"
    echo "  -h      Display help"
    exit 1
}

# Input Handler
source "${DIR}/input-handler.src"

is_in_tty="$( tty -s; echo "$?" )"

echo Is TTY: $( [ $is_in_tty -eq 0 ] && echo 'Yes' || 'No' )

# Current Window PID
c_win_pid="$(xdotool getwindowfocus getwindowpid)"
echo $c_win_pid

# Get Window ID (not PID) from current window
c_win_id=$(wmctrl -l -p | grep $c_win_pid -m 1 | cut  -d ' ' -f1)
echo $c_win_id

# Select webcam device
[ -z "$device" ] \
	&& device="$( . "${DIR}"/lswc.sh )" \
	|| device="$( v4l2-ctl --list-devices | awk -v name="$device" '$0~name{getline;sub(/^[ \t]+/, ""); print}' )"

echo Device: $device

[ ! -c "$device" ] && exit 1

# Set resolution

function display_menu(){
	stdin="$( cat - )"
	if [ "$is_in_tty" -eq 0 ]; then
		#fzf --with-nth 2.. <<< "$stdin"
		fzf <<< "$stdin"
	else
		rofi -dmenu -i <<< "$stdin"
	fi
}

[ -z "$res" ] && \
	res=$( echo "$( \
		v4l2-ctl -d "$device" --list-formats-ext | \
		grep "Size" | \
		cut -d ' ' -f3 | \
		sort -rnu \
	)" | display_menu )

echo Resolution: $res

if [[ "$res" != *"width"* ]]; then
    width="${res%%x*}"
    height="${res##*x}"
    res="width=$width,height=$height"
fi

v4l2-ctl -d "$device" --set-fmt-video="${res},pixelformat=2"

# Show image
mpv \
	--demuxer-lavf-format=video4linux2 \
	--demuxer-lavf-o-set=input_format=mjpeg av://v4l2:/"$device" \
	--no-audio \
	--border=no \
	--geometry="${size}%+3700-85" \
	--profile=low-latency \
	--untimed \
	--no-correct-pts \
	--video-latency-hacks=yes \
	--framedrop=no \
	--speed=1.01 \
	--opengl-glfinish=yes \
	--opengl-swapinterval=0 \
	--vf="$vf" \
	--input-ipc-server=/tmp/mpvsocket \
	--ontop \
& (
	sleep 0.5

	[ "$fullscreen" == "true" ] && socat - /tmp/mpvsocket <<< '{ "command": ["set_property", "fullscreen", true] }' &>/dev/null 
	
	# Loop until mpv is closed
	while [ true ]; do

		# Ping
		socat - /tmp/mpvsocket <<< '{ "command": ["get_property", "fullscreen"] }' &>/dev/null 
		[ $? -gt 0 ] && exit

		# Check fullscreen
		is_fullscreen="$( socat - /tmp/mpvsocket <<< '{ "command": ["get_property", "fullscreen"] }' | jq .data )"
	
		# If fullscreen, apply video filter vf_fs, else, apply video filter vf_normal
		socat - /tmp/mpvsocket &>/dev/null <<< "{ \"command\": [\"set_property\", \"vf\", \"$( [ "$is_fullscreen" == "true" ] && echo "$vf_fs" || echo "$vf_normal" )\"] }"
	done

	exit
) & sleep 0.3
